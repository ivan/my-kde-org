# Generated by Django 3.0.9 on 2020-08-22 12:21

from django.db import migrations, models

def create_full_name(apps, schema_editor):
    User = apps.get_model("bid_main", "User")
    for user  in User.objects.all():
        user.full_name = user.first_name + ' ' + user.last_name
        user.save()


class Migration(migrations.Migration):

    dependencies = [
        ('bid_main', '0041_oauth2application_authorized_roles'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='full_name',
            field=models.CharField(db_index=True, default='', max_length=80, verbose_name='Full Name'),
            preserve_default=False,
        ),
        migrations.RunPython(create_full_name),
        migrations.RemoveField(
            model_name='user',
            name='first_name',
        ),
        migrations.RemoveField(
            model_name='user',
            name='last_name',
        ),
    ]
