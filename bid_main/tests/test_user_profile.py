from django.test import TransactionTestCase


class AutoCreateUserProfileTest(TransactionTestCase):
    def test_create_user_happy_flow(self):
        from django.contrib.auth import get_user_model

        user_cls = get_user_model()
        user = user_cls(email='example@example.com',
                        full_name='Dr. Examplović')
        user.save()

        self.assertEqual('Dr. Examplović', user.full_name)

        return user
