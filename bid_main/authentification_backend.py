# SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-LicenseRef: GPL-3.0-or-later

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from ldap3 import Server, Connection
from datetime import date
from django.contrib import messages
from ldap3.core.exceptions import LDAPException
from django.contrib.auth import get_user_model
from django.db.models import Q

from . import forms, models


class KDEAuthentificationBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None):
        """
        Authentificate the user based on the database and fallback to
        LDAP if it doesn't exist
        """
        user_model = get_user_model()

        if username is None:
            username = kwargs.get(user_model.USERNAME_FIELD)

        # The `username` field is allows to contain `@` characters so
        # technically a given email address could be present in either field,
        # possibly even for different users, so we'll query for all matching
        # records and test each one.
        users = user_model._default_manager.filter(
            #Q(**{user_model.USERNAME_FIELD: username}) | Q(email__iexact=username)
            Q(nickname__iexact=username) | Q(email__iexact=username)
        )

        # Test whether any matched user has the provided password:
        for user in users:
            if user.check_password(password):
                return user
        if not users:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (see
            # https://code.djangoproject.com/ticket/20760)
            user_model().set_password(password)

            # fallback to old identity ldap database
            return self.ldap_login(username, password)
        
    def ldap_login(self, username: str, password: str):
        server = Server('ldap://localhost:389')

        usernameMatch = False

        try:
            conn = Connection(server, 'uid=' + username + ',ou=people,dc=kde,dc=org',
                              password, auto_bind=True)
            usernameMatch = True
        except LDAPException:
            try:
                # try with email instead of username
                conn = Connection(server, 'mail=' + username + ',ou=people,dc=kde,dc=org',
                                  password, auto_bind=True)
            except LDAPException:
                return None

        if conn.search('dc=kde,dc=org', '(uid=' + username + ')' if usernameMatch else '(mail=' + username + ')',
                       attributes=['sn', 'mail', 'givenName', 'uid', 'groupMember', 'secondaryMail']):
            ldap_user = conn.entries[0]

            if models.User.objects.filter(nickname=username,
                                          email=ldap_user.mail.values[0]).count() > 0:
                user = models.User.objects.get(nickname=username,
                                               email=ldap_user.mail.values[0])
            else:
                user = models.User.objects.create_user(ldap_user.mail.values[0], password)

            user.full_name = ldap_user.givenName.values[0] + ' ' + ldap_user.sn.values[0]
            user.nickname = ldap_user.uid.values[0]
            user.confirmed_email_at = date.today()
            user.save()

            # import secondary emails
            for mail in ldap_user.secondaryMail.values:
                models.SecondaryEmail.objects.create(email=mail, user=user, token=None)

            # import groups memberships
            for group in ldap_user.groupMember.values:
                roles = models.Role.objects.filter(name=group)

                if group == 'sysadmins':
                    user.is_staff = True
                    user.is_superuser = True
                    user.save()

                if len(roles) < 1:
                    role = models.Role.objects.create(name=group, description='Fill Description',
                                                      is_active=True, is_badge=False)
                else:
                    role = roles[0]
                user.roles.add(role)

            user.save()
            conn.unbind()
            return user
        return None
