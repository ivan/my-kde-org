from django.conf.urls import url
from django.urls import path

from .views import info, badger

app_name = 'bid_api'
urlpatterns = [
    path('me', info.UserSelfInfoView.as_view(), name='me'),
    path('user', info.UserSelfInfoView.as_view(), name='user'),
    path('userinfo', info.OpenIDUserInfoView.as_view(), name='userinfo'),
    url(r'^user/(?P<user_id>\d+)$', info.UserInfoView.as_view(), name='user-info-by-id'),
    url(r'^user/(?P<user_id>\d+)/avatar$', info.UserAvatarView.as_view(), name='user-avatar'),
    url(r'^badges/(?P<user_id>\d+)$', info.UserBadgeView.as_view(), name='user-badges-by-id'),
    url(r'^badges/(?P<user_id>\d+)/html$', info.BadgesHTMLView.as_view(), name='user-badges-html'),
    url(r'^badges/(?P<user_id>\d+)/html/(?P<size>[a-z])$', info.BadgesHTMLView.as_view(),
        name='user-badges-html'),
    url(r'^stats$', info.StatsView.as_view(), name='stats'),
    url(r'^badger/grant/(?P<badge>[^/]+)/(?P<email_or_uid>[^/]+)$',
        badger.BadgerView.as_view(action='grant'), name='badger_grant'),
    url(r'^badger/revoke/(?P<badge>[^/]+)/(?P<email_or_uid>[^/]+)$',
        badger.BadgerView.as_view(action='revoke'), name='badger_revoke'),
]

# noinspection PyUnresolvedReferences
from . import signals as _
