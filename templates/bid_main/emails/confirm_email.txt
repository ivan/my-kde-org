{% autoescape off %}
Hi {{ user.full_name|default:user.email_to_confirm }}!

You have received this message because your email address
{{ user.email_to_confirm }} has been registered on KDE Account.

Follow this link to confirm it was really you:

{{ url }}

Note that this link will be valid for only 12 hours.

Kind regards,

The KDE Sysadmin team
{% endautoescape %}
