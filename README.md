# KDE Account System

[KDE Account System](https://my.kde.org/) is the unified login system for all services around
KDE. With just one account you can set up a KDE developer account, 

Requires Python 3.6, Django 3.0,
[Django-OAuth-Toolkit](https://django-oauth-toolkit.readthedocs.io/), and a database.

This is a fork of the Blender ID system.

## Documentation

Docs is available in the docs folder.

## LICENSE

This is released under the GPL 3.0
